import Foundation

class LightbarController: AudioMonitorDelegate {
	
	var baseColor: String? = nil
	
	private var audioMonitor: AudioMonitor!
	private var lightbarFiles: [UnsafeMutablePointer<FILE>]!
	
	private static let lightbarBasePath = "/sys/class/leds/"
	private static let lightbarSubpath = "/brightness"
	private static let maxBrightness = 255
	
	private var baseColorValues = [
		LightbarController.maxBrightness,
		LightbarController.maxBrightness,
		LightbarController.maxBrightness
	]
	
	init(lightbarDevice: String) {
		audioMonitor = AudioMonitor()
		audioMonitor.registerObserver(self)
		
		let redFile   = fopen(LightbarController.lightbarBasePath + lightbarDevice + ":red"   + LightbarController.lightbarSubpath, "w")
		let greenFile = fopen(LightbarController.lightbarBasePath + lightbarDevice + ":green" + LightbarController.lightbarSubpath, "w")
		let blueFile  = fopen(LightbarController.lightbarBasePath + lightbarDevice + ":blue"  + LightbarController.lightbarSubpath, "w")
		if (redFile == nil || greenFile == nil || blueFile == nil) {
			print("Can't open file \(LightbarController.lightbarBasePath + ":red" + LightbarController.lightbarSubpath): \(errno)")
			exit(1)
		}
		lightbarFiles = [redFile!, greenFile!, blueFile!]
	}
	
	func start(sink: String?) {
		audioMonitor.startListening(sink: sink)
		
		if baseColor != nil {
			// make sure there's no # on the front
			let s = baseColor!.replacingOccurrences(of: "#", with: "")
			// ugh, Swift, why
			// anyway, it splits into e.g. [FF][FF][FF]
			var index = s.startIndex
			baseColorValues[0] = Int(s[index..<s.index(index, offsetBy: 2)], radix: 16)!
			index = s.index(index, offsetBy: 2)
			baseColorValues[1] = Int(s[index..<s.index(index, offsetBy: 2)], radix: 16)!
			index = s.index(index, offsetBy: 2)
			baseColorValues[2] = Int(s[index..<s.index(index, offsetBy: 2)], radix: 16)!
		}
	}
	
	func receiveVolumeLevel(_ volume: Double) {
		print("\r\(volume) \u{1B}[K", terminator: "")
		
		let r = Int(volume * Double(baseColorValues[0]))
		let g = Int(volume * Double(baseColorValues[1]))
		let b = Int(volume * Double(baseColorValues[2]))
		
		fputs("\(r)", lightbarFiles[0])
		fputs("\(g)", lightbarFiles[1])
		fputs("\(b)", lightbarFiles[2])
		for file in lightbarFiles {
			fflush(file)
		}
		
		fflush(stdout)
	}
	
	deinit {
		audioMonitor = nil
		for file in lightbarFiles {
			fclose(file)
		}
	}
	
}
