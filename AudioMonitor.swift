import Foundation
import PulseAudio

class AudioMonitor {
	
	public var refreshTime: UInt32 = 10_000 // µs
	public var bufferSize = 44100 / 32
	public var sampleRate = 44100
	
	private var listeners: [AudioMonitorDelegate] = []
	
	private var pulseaudio: OpaquePointer? = nil
	private var readQueue: DispatchQueue? = nil
	
	private var currentRawData: [Int16] = []
	private var currentVolume: Double = 0
	
	func startListening(sink: String?) {
		var sampleSpec = pa_sample_spec(format: PA_SAMPLE_S16LE, rate: UInt32(sampleRate), channels: 1)
		var errorID: Int32 = 0
		pulseaudio = pa_simple_new(
			nil,
			"lightbarpulse",
			PA_STREAM_RECORD,
			sink,
			"Audio visualization",
			&sampleSpec,
			nil,
			nil,
			&errorID
		)
		guard pulseaudio != nil else {
			fputs("PulseAudio connection failed: \(String(cString: pa_strerror(errorID)))", stderr)
			return
		}
		
		if self.readQueue == nil {
			self.readQueue = DispatchQueue.global(qos: .userInteractive)
		}
		self.readQueue!.async { [unowned self] in
			while true {
				var data: [Int16] = Array(repeating: 0, count: self.bufferSize/2)
				var errorID: Int32 = 0
				var result = pa_simple_flush(self.pulseaudio, &errorID)
				if result < 0 {
					fputs("PulseAudio read failed: \(String(cString: pa_strerror(errorID)))\n", stderr)
				}
				errorID = 0
				result = pa_simple_read(self.pulseaudio, &data, self.bufferSize, &errorID)
				if result < 0 {
					fputs("PulseAudio read failed: \(String(cString: pa_strerror(errorID)))\n", stderr)
				}
				// fputs("\(data)\n", stderr)
				
				self.currentRawData = data
				
				// find volume and broadcast
				self.calcVolume(data: data)
				
				usleep(self.refreshTime)
			}
		}
	}
	
	func registerObserver(_ o: AudioMonitorDelegate) {
		listeners.append(o)
	}
	func unregisterObserver(_ o: AudioMonitorDelegate) {
		for i in 0..<listeners.count {
			if listeners[i] === o {
				listeners.remove(at: i)
				break
			}
		}
	}
	func broadcastData() {
		for listener in listeners {
			listener.receiveWaveformData(currentRawData)
			listener.receiveVolumeLevel(currentVolume)
		}
	}
	
	func calcVolume(data: [Int16]) {
		// calculate the RMS
		if data.count == 0 {
			currentVolume = 0
		} else {
			currentVolume = sqrt(
				Double(data.map({ (x) in
					return Int(x) * Int(x)
				}).reduce(0, +)) / Double(data.count)
			)
		}
		
		currentVolume /= Double(Int16.max)
		// boost volume to make the light reasonably bright
		currentVolume *= 2
		
		broadcastData()
	}
	
	deinit {
		// NOTE: This is NOT necessarily called when the program exits!
		// The AudioMonitor has to be set to nil somewhere.
		pa_simple_free(pulseaudio)
	}
	
}
