import Foundation

var lightbar: String!
var sink: String? = nil
var basecolorstr: String? = nil

if CommandLine.arguments.count <= 1 {
	fputs("Usage: lightbarpulse lightbar-device [audio-source [base-color]]", stderr)
	exit(1)
}

lightbar = CommandLine.arguments[1]

if CommandLine.arguments.count > 2 {
	sink = CommandLine.arguments[2]
}
if CommandLine.arguments.count > 3 {
	basecolorstr = CommandLine.arguments[3]
}

let lightbarController = LightbarController(lightbarDevice: lightbar)
lightbarController.baseColor = basecolorstr
lightbarController.start(sink: sink)

// stall forever
while (true) {
	sleep(1000)
}
