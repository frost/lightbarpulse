#
# Makefile for lightbarpulse
# vim:set fo=tcqr:
#

MODULEIMPORT=-I${PWD}/modules/pulseaudio

lightbarpulse: *.swift
	swiftc -g -o lightbarpulse *.swift ${MODULEIMPORT}

clean:
	rm -f lightbarpulse
	rm -rf lightbarpulse.dSYM/
