# lightbarpulse

Make your PS4 controller's lightbar react to your music!

It only works on Linux, since Linux provides an easy way to get to the lightbar LEDs. It also only works with PulseAudio (although this would be much easier to change).

## Compilation

You'll need Swift. You can get it from https://swift.org.

You'll also need the PulseAudio development files. On Ubuntu,
you should be able to get these through apt:
```
sudo apt install libpulse-dev
```

Then just run `make`.

## Usage

```
lightbarpulse lightbar-device [source [color]]
```

The lightbar device will be something like `/sys/class/leds/0003:054C:09CC.000B:red` (as well as green and blue) - only include the `0003:054C:09CC.000B` part.

lightbarpulse can take a PulseAudio source name to listen to; if you want to
monitor an output, append `.monitor` to the output sink name. You can get
the available outputs with this command:

```
pacmd list-sinks | grep 'name:' | sed -Ee 's/.*<(.*)>.*/\1.monitor/'
```

If no source is given, it defaults to whatever source you have set as default,
typically your microphone input.

## License

This is free and unencumbered software released into the public domain.
See [COPYING.md](COPYING.md) for more information.
