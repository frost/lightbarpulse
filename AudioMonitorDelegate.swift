//
// AudioMonitorDelegate.swift
// pavisualizer
//
// Created by ArgentWolf on 2020-02-16
//

protocol AudioMonitorDelegate: class {
	
	func receiveVolumeLevel(_ volume: Double)
	func receiveWaveformData(_ data: [Int16])
	
}

// default implementations
extension AudioMonitorDelegate {
	func receiveVolumeLevel(_ volume: Double) {
		
	}
	func receiveWaveformData(_ data: [Int16]) {
		
	}
}
